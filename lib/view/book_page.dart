import 'package:bookshelf/controller/book_controller.dart';
import 'package:bookshelf/global.dart';
import 'package:bookshelf/view/image_widget.dart';
import 'package:flutter/material.dart';
import 'package:home_widget/home_widget.dart';

class BookPage extends StatefulWidget {
  const BookPage({super.key});

  @override
  State<BookPage> createState() => _BookPageState();
}

class _BookPageState extends State<BookPage> {
  BookController bookController = BookController();

  @override
  void initState() {
    super.initState();
    HomeWidget.setAppGroupId('YOUR_GROUP_ID');
    HomeWidget.registerBackgroundCallback(backgroundCallback);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Future.delayed(const Duration(seconds: 1), () {
        bookController.sendAndUpdate();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomeWidget'),
      ),
      body: GridView(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 186.67 / 280,
          mainAxisSpacing: 10,
          crossAxisSpacing: 5,
        ),
        children: const [
          ImageWidget(imagePath: "assets/spain.jpeg"),
          ImageWidget(imagePath: "assets/roman.jpg"),
          ImageWidget(imagePath: "assets/switzerland.jpg"),
        ],
      ),
    );
  }
}
