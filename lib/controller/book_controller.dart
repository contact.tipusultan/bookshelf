import 'package:bookshelf/view/image_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:home_widget/home_widget.dart';

class BookController {
  Future _sendData() async {
    try {
      return Future.wait([
        HomeWidget.renderFlutterWidget(
          const ImageWidget(imagePath: "assets/spain.jpeg"),
          logicalSize: const Size(186.67, 280),
          key: 'image1',
        ),
        HomeWidget.renderFlutterWidget(
          const ImageWidget(imagePath: "assets/roman.jpg"),
          logicalSize: const Size(186.67, 280),
          key: 'image2',
        ),
        HomeWidget.renderFlutterWidget(
          const ImageWidget(imagePath: "assets/switzerland.jpg"),
          logicalSize: const Size(186.67, 280),
          key: 'image3',
        ),
      ]);
    } on PlatformException catch (exception) {
      debugPrint('Error Sending Data. $exception');
    }
  }

  Future _updateWidget() async {
    try {
      return HomeWidget.updateWidget(
        name: 'HomeBookWidgetProvider',
        iOSName: 'HomeBookWidgetProvider',
      );
    } on PlatformException catch (exception) {
      debugPrint('Error Updating Widget. $exception');
    }
  }

  Future<void> sendAndUpdate() async {
    await _sendData();
    await _updateWidget();
  }
}
