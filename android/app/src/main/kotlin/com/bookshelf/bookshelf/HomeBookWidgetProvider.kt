package com.bookshelf.bookshelf

import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.net.Uri
import android.view.View
import android.widget.RemoteViews
import es.antonborri.home_widget.HomeWidgetLaunchIntent
import es.antonborri.home_widget.HomeWidgetProvider

class HomeBookWidgetProvider : HomeWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray, widgetData: SharedPreferences) {
        appWidgetIds.forEach { widgetId ->
            val views = RemoteViews(context.packageName, R.layout.book_widget_list).apply {
                // Open App on Widget Click
                val pendingIntent = HomeWidgetLaunchIntent.getActivity(
                    context,
                    MainActivity::class.java,Uri.parse("bookshelf://bookshelf/showAll"))
                setOnClickPendingIntent(R.id.show_more, pendingIntent)

                val image1 = widgetData.getString("image1", null)
                if (image1 != null) {

                    setImageViewBitmap(R.id.widget_img_1st, BitmapFactory.decodeFile(image1))
                    setViewVisibility(R.id.widget_img_1st, View.VISIBLE)
                } else {
                    setViewVisibility(R.id.widget_img_1st, View.GONE)
                }


                val image2 = widgetData.getString("image2", null)
                if (image1 != null) {

                    setImageViewBitmap(R.id.widget_img_2nd, BitmapFactory.decodeFile(image2))
                    setViewVisibility(R.id.widget_img_2nd, View.VISIBLE)
                } else {
                    setViewVisibility(R.id.widget_img_2nd, View.GONE)
                }

                val image3 = widgetData.getString("image3", null)
                if (image3 != null) {

                    setImageViewBitmap(R.id.widget_img_3rd, BitmapFactory.decodeFile(image3))
                    setViewVisibility(R.id.widget_img_3rd, View.VISIBLE)
                } else {
                    setViewVisibility(R.id.widget_img_3rd, View.GONE)
                }
            }

            appWidgetManager.updateAppWidget(widgetId, views)
        }
    }
}